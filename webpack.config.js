var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
	entry: './js/main.js',
	output: {
		path: './',
		filename: 'build/index.js'
	},
	devServer: {
		inline: true,
		port: 3333
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel',
				query: {
					presets: ['es2015', 'react']
				}
			},
			{
				test: /\.scss$/,
				loader: ExtractTextPlugin.extract('style-loader', ['css-loader', 'sass-loader'])
			}
		]
	},
	plugins: [
    new ExtractTextPlugin('style.css', {
      allChunks: true
    })
  ]
}