import React from 'react'
import { Router, Route, Link } from 'react-router'


class Header extends React.Component {

  render() {
    return (
      <div className="mdl-layout mdl-layout--fixed-header">
        <header className="mdl-layout__header">
          <div className="mdl-layout__header-row">
            <nav className="mdl-navigation">
              <Link className="mdl-navigation__link" to="/">Home</Link>
              <Link className="mdl-navigation__link" to="/create">Create</Link>
            </nav>
          </div>
        </header>
      </div>
    )
  }
}

export default Header