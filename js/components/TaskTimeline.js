import React from 'react'

export default class TaskTimeline extends React.Component {
  constructor (props) {
    super(props);
  }

  getTotalSum() {
    let sum = 0;
    const tasks = this.props.tasks;

    for (let i = 0; i < tasks.length; i++) {
      sum += tasks[i].duration;
    }

    return sum;
  }

  createBox(task) {
    let totalSum = this.getTotalSum();
    let width = (task.duration/totalSum) * 100;
    let style = {
      width: width + '%'
    };
    return <div key={task.id} className="timeline__item" style={style}>{task.text}</div>
  }

  createTimeline() {
    return this.props.tasks.map((task) => {
      return this.createBox(task);
    })
  }

  updatePorgressBar(progress) {
    return {
      width: progress + '%'
    }
  }

  render () {
    const Timeline = this.createTimeline();
    let progressBar = (
      <div className="hidden"></div>
    )
    if (this.props.position) {
      let progressBarStyle = this.updatePorgressBar(this.props.position);
      progressBar = (
        <div className="timeline__progress-bar" style={progressBarStyle}></div>
      )
    }

    return (
      <div className="timeline">
        { progressBar }
        { Timeline }
      </div>
    )
  }
}
