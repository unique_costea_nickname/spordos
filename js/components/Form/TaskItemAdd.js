import React from 'react'
import dispatcher from '../../dispatcher';

export default class TaskItemAdd extends React.Component {
  constructor(props) {
    super(props);
  }

  changeHandler(type, event) {
    dispatcher.dispatch({
      type: 'UPDATE_FIELDS',
      value: event.target.value,
      id: this.props.id,
      field_type: type
    });
  }

  onDeleteInput(event) {
    event.preventDefault();
    dispatcher.dispatch({type: 'REMOVE_INPUT', id: event.target.id })
  }

  render() {
    return (
      <div className="task-item-add">
        <div className="mdl-textfield">
          <input className="mdl-textfield__input" type="text" placeholder={this.props.placeholder} defaultValue={this.props.text} onChange={this.changeHandler.bind(this, 'text')}/>
          <button className="mdl-button mdl-button--raised mdl-button--accent" id={this.props.id} onClick={this.onDeleteInput.bind(this)}>Delete</button>
        </div>
        <div className="mdl-textfield mdl-textfield--half">
          <input className="mdl-textfield__input" type="number" defaultValue={this.props.duration} onChange={this.changeHandler.bind(this, 'duration')} placeholder="Duration" />
          <span className="duration">Seconds</span>
        </div>
      </div>
    )
  }
}