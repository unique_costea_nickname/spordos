import React from 'react'
import dispatcher from '../../dispatcher';


export default class InputField extends React.Component {

  changeHandler(event) {
    dispatcher.dispatch({type: 'UPDATE_ITEM', value: event.target.value});
  }

  render() {
    return (
      <div className="mdl-textfield">
        <input className="mdl-textfield__input" type="text" placeholder={this.props.placeholder} value={this.props.title} onChange={this.changeHandler.bind(this)} />
      </div>
    )
  }
}