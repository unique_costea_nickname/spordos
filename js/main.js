import React from 'react'
import ReactDom from 'react-dom'
import { Router, Route, Link, hashHistory, IndexRoute } from 'react-router'
import Header from './components/Header'
import Home from './pages/Home'
import CreatePage from './pages/CreatePage'
import Item from './pages/Item'


import styles from '../scss/main.scss'


const app = document.getElementById('app');


class App extends React.Component {
  render() {
    return (
      <div>
        <Header/>
        <main className="main-block">
          {this.props.children}
        </main>
      </div>
    )
  }
}


ReactDom.render((
  <Router history={hashHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Home} />
      <Route path="items/:itemId" component={Item}></Route>
      <Route path="edit/:itemId" component={CreatePage}></Route>
      <Route path="create" component={CreatePage}></Route>
    </Route>
  </Router>
), app);