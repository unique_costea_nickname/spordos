import React from 'react'
import TaskTimeline from '../components/TaskTimeline'
import InputField from '../components/form/InputField'
import TaskItemAdd from '../components/form/TaskItemAdd'
import FormStore from '../stores/FormStore'


class CreatePage extends React.Component {
  constructor(props) {
    super(props);
    FormStore.setInitial(props);


    if (props.params.itemId) {
      FormStore.updateState(props.params.itemId);
    }

    this.state = {
      item: FormStore.getItem(),
      meta: {
        inputs: FormStore.getMeta()
      }
    };

    this.getInputs = this.getInputs.bind(this);
  }

  componentWillMount () {
    FormStore.on('update', this.getInputs);
  }

  componentWillUnmount () {
    FormStore.removeListener('update', this.getInputs);
  }

  appendItem() {
    FormStore.appendInput();
  }

  getInputs() {
    this.setState({
      item: FormStore.getItem(),
      meta: {
        inputs: FormStore.getMeta()
      }
    })
  }

  submitHandler(event) {
    event.preventDefault();
    FormStore.submitForm(event)
  }

  render() {
    let { item } = this.state;
    let { inputs } = this.state.meta;

    const InputListComponent = inputs.map((input) => {
      return <TaskItemAdd key={input.id} text={input.text} duration={this.duration} placeholder={input.placeholder} {...input}/>
    });

    return (
      <form action="#" onSubmit={this.submitHandler.bind(this)}>
        <InputField placeholder="Title" title={item.title} />

        <TaskTimeline tasks={inputs} />

        <div className="list">
          {InputListComponent }
        </div>

        <button type="button" className="mdl-button mdl-button--raised mdl-button--accent" onClick={this.appendItem}>Add Task</button>
        <button type="submit" className="mdl-button mdl-button--primary">
          Save
        </button>
      </form>
    )
  }
}

export default CreatePage