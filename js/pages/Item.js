import React from 'react'
import SingleItemStore from '../stores/SingleItemStore'
import TaskTimeline from '../components/TaskTimeline'
import { Link } from 'react-router'

class Item extends React.Component {
  constructor(props) {
    super(props);

    SingleItemStore.setInitial();

    this.itemId = this.props.params.itemId;

    this.state = {
      loading: true,
      item: SingleItemStore.fetchItem(this.itemId),
      playing: 'Play',
      progress: SingleItemStore.progressStatus
    }

    this.fetchItem = this.fetchItem.bind(this);
    this.updateProgress = this.updateProgress.bind(this);
  }


  componentWillMount () {
    SingleItemStore.on('update', this.fetchItem);
    SingleItemStore.on('progress', this.updateProgress);
  }

  componentWillUnmount () {
    SingleItemStore.removeListener('update', this.fetchItem);
    SingleItemStore.removeListener('progress', this.updateProgress);
  }

  fetchItem() {
    this.setState({
      loading: false,
      item: SingleItemStore.getItem()
    });
  }

  updateProgress() {
    let playLabel = 'Play';

    if (SingleItemStore.state.playing) {
      playLabel = 'Pause';
    }

    this.setState({
      progress: SingleItemStore.progressStatus,
      playing: playLabel
    })
  }

  togglePlay(event) {
    event.preventDefault();
    if (SingleItemStore.state.playing) {
      SingleItemStore.stop()
    }else {
      SingleItemStore.play();
    }
  }

  resetTimeline(event) {
    event.preventDefault();
    SingleItemStore.reset();
  }

  render() {
    const { item } = this.state;
    let { progress } = this.state;
    let { playing } = this.state;

    if (!item) return false;

    return (
      <div className="single-item">
        <h2 className="mdl-layout-title">{item.title} <Link className="mdl-list__item-primary-content single-item__edit-link" to={'edit/' + this.itemId}>Edit</Link></h2>
        <TaskTimeline tasks={item.metadata} position={progress} />
        <button type="button" className="mdl-button mdl-button--raised mdl-button--accent" onClick={this.togglePlay.bind(this)}>{playing}</button>
        <button type="button" className="mdl-button mdl-button--raised mdl-button--accent" onClick={this.resetTimeline.bind(this)}>Reset</button>
      </div>
    )
  }
}

export default Item