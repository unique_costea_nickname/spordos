import React from 'react'
import ItemsStore from '../stores/ItemsStore'
import { Link } from 'react-router'

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      items: ItemsStore.fetchList()
    }

    this.fetchList = this.fetchList.bind(this);
  }

  componentWillMount () {
    ItemsStore.on('update', this.fetchList);
  }

  componentWillUnmount () {
    ItemsStore.removeListener('update', this.fetchList);
  }

  fetchList() {
    this.setState({
      items: ItemsStore.getList(),
    })
  }

  render() {
    const { items } = this.state;

    if (!items) return false;

    const List = Object.keys(items).map((key) => {
      let path = 'items/' + key;
      let duration = 0;
      for (var i = items[key].metadata.length - 1; i >= 0; i--) {
        duration += items[key].metadata[i].duration
      }
      let badge = duration + 's';
      return (
        <li className="mdl-list__item" key={key}>
          <Link className="mdl-list__item-primary-content" to={path}><span className="mdl-badge" data-badge={ badge }>{ items[key].title }</span></Link>
        </li>
      )
    })
    return (
      <div className="homepage">
        <ul className="mdl-list list-items">
          { List }
        </ul>
      </div>
    )
  }
}

export default Home