import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyAEd8DvcwA7PcmsctnoxcJiLX7BElcM7lU",
  authDomain: "spordos-a57bf.firebaseapp.com",
  databaseURL: "https://spordos-a57bf.firebaseio.com",
  storageBucket: "spordos-a57bf.appspot.com",
  messagingSenderId: "1011499031904"
};
firebase.initializeApp(firebaseConfig);

let database = firebase.database();

export default database;