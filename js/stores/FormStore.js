import { EventEmitter } from 'events';
import dispatcher from '../dispatcher';
import database from '../firebase';


class FormStore extends EventEmitter {
  constructor(props) {
    super(props)

    this.setInitial(props)
  }

  setInitial(props) {
    this.item = {
      id: Date.now(),
      title: ''
    }

    this.meta = {
      inputs: [
        {
          id: Date.now(),
          placeholder: 'Task 1',
          text: '',
          duration: 1
        }
      ]
    }
  }

  updateState(itemId) {
    const self = this;
    this.item.id = itemId;

    database.ref('/items/' + itemId).once('value').then((snapshot) => {
      let result = snapshot.val();
      self.item = {
        id: result.id,
        title: result.title
      }

      self.meta = {
        inputs: result.metadata
      }
      self.emit('update');
    });
  }

  appendInput() {
    const id = Date.now() + 1;
    const length = this.meta.inputs.length + 1;
    this.meta.inputs.push({
      id: id,
      placeholder: 'Task ' + length,
      text: '',
      duration: ''
    });

    this.emit('update');
  }

  removeInput(id) {
    this.meta.inputs = this.meta.inputs.filter((element) => {
      return element.id != id;
    })

    setTimeout(() => {
      this.emit('update');
    }, 0)


    // if (this.meta.inputs.length < 1) {
    //   this.appendInput();
    // }

  }

  getItem() {
    return this.item;
  }

  getMeta() {
    return this.meta.inputs;
  }

  updateTimeline(action) {
    let field_type = action.field_type;

    if (field_type == 'text') {
      this._updateText(action.id, action.value);
    }else if(field_type == 'duration') {
      this._updateDuration(action.id, action.value);
    }

    this.emit('update');
  }

  _updateText(id, text) {
    this.meta.inputs = this.meta.inputs.filter((element) => {
      if (element.id == id) {
        element.text = text;
      }
      return element;
    });
  }

  _updateDuration(id, time) {
    this.meta.inputs = this.meta.inputs.filter((element) => {
      if (element.id == id) {
        element.duration = parseInt(time, 10);
      }
      return element;
    });
  }

  updateItem(value) {
    this.item.title = value;
    this.emit('update');
  }

  submitForm(event) {
    const item = this.item;
    const meta = this.meta;

    let isDone = database.ref('items/'+item.id).set({
      id: item.id,
      title: item.title,
      metadata: meta.inputs
    }).then(() => {
      this.emit('update');
    });

  }

  handleAction(action) {
    switch(action.type) {
      case 'APPEND_INPUT': {
        this.appendInput()
        break;
      }
      case 'REMOVE_INPUT': {
        this.removeInput(action.id)
        break;
      }
      case 'UPDATE_FIELDS': {
        this.updateTimeline(action);
        break;
      }
      case 'UPDATE_ITEM': {
        this.updateItem(action.value);
        break;
      }
    }
  }
}

const formStore = new FormStore;
dispatcher.register(formStore.handleAction.bind(formStore))

export default formStore;