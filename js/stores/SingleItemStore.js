import { EventEmitter } from 'events';
import dispatcher from '../dispatcher';
import database from '../firebase';
import speaker from '../speaker';


class SingleItemStore extends EventEmitter {
  constructor(props) {
    super(props);

    this.setInitial(props);
  }

  setInitial(props) {
    this.state = {
      loading: true,
      playing: false,
      item: null
    }

    this.count = 0;
    this.progress = null;
    this.progressStatus = 0;
    this.duration = {
      total: 0,
      breakpoints: []
    };
  }

  // Ajax Calls
  fetchItem(id) {
    let self = this;

    database.ref('/items/' + id).once('value').then((snapshot) => {
      self.state = {
        loading: false,
        item: snapshot.val()
      }
      self.setTotalDuration();
      self.emit('update');
    })
  }

  getItem() {
    return this.state.item;
  }

  // Just Player Things
  setTotalDuration() {
    this.state.item.metadata.map((element) => {
      this.duration.total += element.duration;
      this.duration.breakpoints.push(element.duration);
    })
  }

  getConvertedUnits() {
    return 10 / this.duration.total;
  }

  getBreakPoints() {
    return this.duration.breakpoints;
  }

  updateProgressBar() {
    const unit = this.getConvertedUnits();

    this.progressStatus += unit;
    this.state.playing = true;
    this.checkBreakPoint();
    this.emit('progress');
  }

  play() {
    const self = this;

    if(this.state.playing) {
      this.stop();
      return false;
    }

    if (!this.state.playing) {
      this.speak();
    }

    this.updateProgressBar();
    this.progress = setInterval(() => {

      if (self.progressStatus >= 100) {
        self.stop();
        return false;
      }
      self.updateProgressBar()
    }, 100);
  }

  stop() {
    this.state.playing = false;
    clearInterval(this.progress);
    this.emit('progress');
  }

  speak() {
    const { metadata } = this.state.item;
    speaker.speak(metadata[this.count].text);
  }

  reset() {
    this.state.playing = false;
    this.progressStatus = 0;
    this.count = 0;
    clearInterval(this.progress);
    this.emit('progress');
  }

  checkBreakPoint() {
    const breakpoints = this.duration.breakpoints;
    const currentStatus = this.progressStatus;
    const self = this;

    let nextPosition = 0;

    for (let i = this.count; i >= 0; i--) {
      nextPosition += (breakpoints[i]/this.duration.total) * 100;
    }
    nextPosition -= .35;

    if (currentStatus >= nextPosition) {
      this.stop();
      this.count++;

      // break at the end;
      if (nextPosition >= 100) return false;

      setTimeout(() => {
        self.play();
      }, 1000)
    }
  }

  handleAction(action) {
    switch(action.type) {
      case 'RECEIVE_ITEM': {
        this.fetchItem(action.id)
        break;
      }
      case 'FETCH_ITEM': {
        this.getItem();
        break;
      }
    }
  }
}



const singleItemStore = new SingleItemStore();
dispatcher.register(singleItemStore.handleAction.bind(singleItemStore))

export default singleItemStore;