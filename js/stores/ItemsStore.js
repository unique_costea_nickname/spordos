import { EventEmitter } from 'events';
import dispatcher from '../dispatcher';
import database from '../firebase';


class ItemsStore extends EventEmitter {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      data: null
    }
  }

  fetchList() {
    let self = this;
    database.ref('items').once('value').then((snapshot) => {
      self.state = {
        loading: false,
        data: snapshot.val()
      }
      self.emit('update');
    });

  }

  getList() {
    return this.state.data;
  }

  handleAction(action) {
    switch(action.type) {
      case 'RECEIVE_LIST': {
        this.fetchList()
        break;
      }
      case 'FETCH_LIST': {
        this.getList();
        break;
      }
    }
  }
}


const itemsStore = new ItemsStore();
dispatcher.register(itemsStore.handleAction.bind(itemsStore))

export default itemsStore;