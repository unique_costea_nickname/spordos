'use strict';

class Speaker {
  constructor() {
    this.msg = new SpeechSynthesisUtterance();
    this.msg.voice = speechSynthesis.getVoices()[7];
    this.msg.text = 'Hello Guys';
    this.text = 'Hello World';
  }

  speak(message) {
    this.msg.text = message;
    speechSynthesis.speak(this.msg);
  }
}

const speaker = new Speaker();
export default speaker;
